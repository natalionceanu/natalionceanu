package com.project.ArrayList;

public class Test {

    public static void main(String[] args) {
        StudentDao studentList = new StudentDaoImpl();

        //print all student
        for (Student student : studentList.getAllStudents()) {
            System.out.println("Student: Number " + student.getNumber() + " Name " + student.getName());
        }

        //update student
        Student student = studentList.getAllStudents().get(2);
        student.setName("Pushok");
        studentList.updateStudent(student);

        //get student
        studentList.getStudent(2);
        System.out.println("New Student: Number " + student.getNumber() + " Name " + student.getName());

        //delete student
        Student student2 = studentList.getAllStudents().get(0);
        studentList.deleteStudent(student2);

        
    }
}
