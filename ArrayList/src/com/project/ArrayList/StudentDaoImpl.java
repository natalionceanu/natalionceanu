package com.project.ArrayList;

import java.util.ArrayList;
import java.util.List;

public class StudentDaoImpl implements StudentDao {

    List<Student> students  = new ArrayList<>();

    StudentDaoImpl() {
        Student student1 = new Student("Jerry", 1);
        Student student2 = new Student("Bob", 2);
        Student student3 = new Student("Philip", 3);
        Student student4 = new Student("Tom", 4);
        Student student5 = new Student("Mateo", 5);
        students.add(student1);
        students.add(student2);
        students.add(student3);
        students.add(student4);
        students.add(student5);
    }

    //retrive list of students
    @Override
    public List<Student> getAllStudents() {
        return students;
    }

    //add student
    @Override
    public Student getStudent(int number) {
        return students.get(number);
    }

    //update list
    @Override
    public void updateStudent(Student student) {
        students.get(student.getNumber()).setName(student.getName());
        System.out.println("Student: Number " + student.getNumber() + " updated in the database");
    }

    //delete student
    @Override
    public void deleteStudent(Student student) {
        students.remove(student.getNumber());
        System.out.println("Student: " + student.getNumber() + " deleted from database");
    }

}
