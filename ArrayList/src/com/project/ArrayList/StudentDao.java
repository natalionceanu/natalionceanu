package com.project.ArrayList;

import java.util.List;

public interface StudentDao {

    public List<Student> getAllStudents();

    public Student getStudent(int number);

    public void updateStudent(Student student);

    public void deleteStudent(Student student);
    
}
