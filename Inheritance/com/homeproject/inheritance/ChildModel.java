package com.homeproject.inheritance;

public class ChildModel extends Model {

    public ChildModel() {
        super(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, 2, 333);

    }

}
