package com.homeproject.inheritance;

import java.util.Arrays;

public class ChildChildModel extends ChildModel {
    int removeId;

    public ChildChildModel() {
        super();
        this.removeId = 3;
    }

    public void Remove() {

        for (int i = removeId; i < myArray.length - 1; i++) {
            myArray[i] = myArray[i + 1];
        }
        System.out.println("After removing: " + Arrays.toString(myArray));
    }
}
