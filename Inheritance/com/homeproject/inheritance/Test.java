package com.homeproject.inheritance;

public class Test {

    public static void main(String[] args) {
        Model mb1 = new ChildModel();
        ChildChildModel cm = new ChildChildModel();
        Model mb2 = new ChildModel2();
        ChildModel2 cm2 = new ChildModel2();

        mb1.Read();
        mb1.Create();
        cm.Remove();
        mb2.Read();
        mb2.Create();
        cm2.Remove();

    }
}
