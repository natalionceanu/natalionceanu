package com.homeproject.inheritance;

import java.util.Arrays;

public class ChildModel2 extends Model {

    int removeId;

    public ChildModel2() {
        super(new int[]{10, 9, 8, 7, 6, 5, 4, 3, 2, 1}, 2, 555);
        this.removeId = 4;
    }

    public void Remove() {

        for (int i = removeId; i < myArray.length - 1; i++) {
            myArray[i] = myArray[i + 1];
        }
        System.out.println("After removing: " + Arrays.toString(myArray));
    }
}
