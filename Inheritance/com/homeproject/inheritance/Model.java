package com.homeproject.inheritance;
import java.util.Arrays;

public class Model {

    int myArray[];
    int idPosition;
    int newValue;

    public Model(int array[], int pos, int val) {
        this.myArray = array;
        this.idPosition = pos;
        this.newValue = val;
    }

    public void Read() {
        System.out.println("Original Array: " + Arrays.toString(myArray));
    }

    public void Create() {
        for (int i = myArray.length - 1; i > idPosition; i--) {
            myArray[i] = myArray[i - 1];
        }
        myArray[idPosition] = newValue;
        System.out.println("New Array: " + Arrays.toString(myArray));
    }

}
