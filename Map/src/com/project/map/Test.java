package com.project.map;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class Test {

    public static void main(String[] args) {

        Map<Integer, String> hmap = new HashMap<>();
        hmap.put(1, "Cat");
        hmap.put(2, "Dog");
        hmap.put(3, "Tiger");
        hmap.put(4, "Lion");
        hmap.put(5, "Cat");

        //forEach to iterate and display each key and value
        hmap.forEach((k, v) -> System.out.println(k + " - " + v));

        //forEach to iterate a Map and display the value of particular key
        hmap.forEach((k, v) -> {
            if (k == 3) {
                System.out.println("Value associated with key 4 is: " + v);
            }
        });

        //foreach to iterate a Map and display the key associated with a particular value
        hmap.forEach((k, v) -> {
            if ("Cat".equals(v)) {
                System.out.println("Key associated with value Cat is: " + k);
            }
        });

        //return all the keys
        Set<Integer> hmapKeys = hmap.keySet();
        System.out.println("Initail key: " + hmapKeys);

        //return all the values
        Collection<String> hmapValues = hmap.values();
        System.out.println("Initial values: " + hmapValues);

        //add new set of key-value
        hmap.put(6, "Mouse");

        //new map with keys and values
        System.out.println("New keys: " + hmap.keySet());
        System.out.println("New values: " + hmap.values());

        //replace
        hmap.replace(5, "Cat", "Kitten");
        System.out.println(hmap);

        Set<Integer> setCodes = hmap.keySet();
        Iterator <Integer> iterator = setCodes.iterator();
        while (iterator.hasNext()){
        Integer k = iterator.next();
        String v = hmap.get(k);
            System.out.println(k + " => " + v);
        }
        
        //remove based on key
        hmap.remove(4);
        System.out.println ("Key and values after removal: ");
        Set set2 = hmap.entrySet();
        Iterator itr = set2.iterator ();
        while (itr.hasNext()){
        Map.Entry mentry = (Map.Entry)itr.next();
        System.out.println("Key is: " + mentry.getKey() + " and value is: " + mentry.getValue());
        }
         
        
    }
}
